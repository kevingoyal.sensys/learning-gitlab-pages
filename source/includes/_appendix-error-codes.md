[comment]: # (#######################################################################################################)
[comment]: # (# 										Appendix Main Group                                        	#)
[comment]: # (#######################################################################################################)

[comment]: # (#######################################################################################################)
[comment]: # (# 											Job Status                                        		#)
[comment]: # (#######################################################################################################)

## Job Status

<aside class="notice">
This section contains all the standard meanings of job status codes applicable throughout the application. 
</aside>

The HRMThread Application API's uses the following job status codes:


Status Code | Meaning | Meaning
----------- | ------- | -------
`P` | Pending | Records are currently being processed
`C` | Completed | All Records have been processed
`CE` | CompletedWithErrors | Some Records could not be processed due to errors
`T` | Terminated | There was an error while processing the request


[comment]: # (#######################################################################################################)
[comment]: # (# 											Error Codes                                        		#)
[comment]: # (#######################################################################################################)

## Error Codes

<aside class="notice">
This section contains all the standard meanings of error codes applicable throughout the application. 
</aside>

The HRMThread Application API's uses the following error codes:


Error Code | Value Returned | Meaning
---------- | -------------- | -------
`Err001` | T | Invalid JSON
`Err002` | CE | Error in records
`Err003` | T | Error while saving in database
`Err004` | T | Critical Error
`Err005` | T | Invalid Token

[comment]: # (#######################################################################################################)
[comment]: # (# 									   JSON Error Codes                                        		#)
[comment]: # (#######################################################################################################)

## JSON Record Error Codes

<aside class="notice">
This section contains all the standard meanings of JSON Record error codes applicable throughout the application. 
</aside>

The HRMThread Application API's uses the following JSON record error codes:


Error Code | Meaning
---------- | -------
`Err101` | Field is mandatory
`Err102` | Duplicate value
`Err103` | Master value not found in database
`Err104` | Invalid Date Format
`Err105` | Invalid attribute
