# How to make API requests?

## Generating OAuth2 Token
Get the TOKEN from OAuth2 Token API of respective user. This TOKEN will be used for all API requests for the current session. The TOKEN is valid for 4 hours only, thereafter new TOKEN needs to be generated.

## Post data to API
Make request to Employee API and pass the Auth Token along with payload as a POST request. Once the API is successfully called you will receive a Job ID in the response which consists of string value.

## Get status of the current API call
Two approaches to check the status of current request – 

1. Make request to JobStatus API and pass the JobID as parameter to get result of earlier operation 

2. Alternatively, provide a Callback URL for each API. This is a one-time configuration and can be done in the settings. 
  
  1. Once the request process is completed it will call the Callback URL given in the API and will send the result of this operation in request body.

<aside class="notice">
<strong>Note:</strong><br /> This "Callback URL" is not the same as "Backposting URL" which will be implemented in future transactional API's. <br /><br />

This "Callback URL" will be immediately hit after the processing of the records unlike a "Backposting URL" which will be called only after processing in the HRMThread application.
</aside>
