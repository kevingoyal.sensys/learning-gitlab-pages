# How to read the API Doc?

This document is written for the developers to make their understanding easy, and fast development and integration process. 

The user needs `TOKEN` and `JobID` from two API’s before sending request to any API - `OAuth2` and `JobID`. 

Certain values, and error codes are practiced throughout the application called “Master Values”, and “Response Codes”. 

Thereafter, you may read respective API description for availing the service.

The request body and response body are in JSON format.

<aside class="notice">
	Note: To get User Token for accessing API’s the user should be registered with the application “HRMThread” with role “Integrate” to access other API’s developed by Sensys Technologies Pvt. Ltd.
</aside>

## Understanding the API Operations

The user can perform multiple operations with this API such as `edit and add`, and `delete`. 

The value in the key `_recordmode` that determines the type of operation. The user performs the appropriate operation with this API by passing respective value in the key `_recordmode`. Refer the table below - 

Value | Type of Operation
----- | -----------------
EA | Edit Add
D | Delete
