---
title: HRMThread - API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - json
  - json--edit-add: Edit & Add
  - json--delete: Delete

toc_footers:
  - <a href='http://www.sensysindia.com/'>Document Owner Sensys Technologies Pvt Ltd</a>

includes:
  - how-to-read-the-api-doc
  - how-to-make-api-requests
  - prerequisite-api.md.erb
  - master-api
  - employee-event
  - other-system
  - appendix
  - appendix-error-codes

search: true
---

# Introduction

Welcome to the HRMThread API! You can use our API to access the HRMThread API endpoints, which can get information on various API's.

You can view code examples in the dark area to the right.

You may click on the tabs visible on top in the dark area to the right to view respective JSON body request and response.

A screenshot of POSTMAN API development environment is attached at the end of every API for reference purposes. You may enlarge the screenshot by right-click on the image and selecting the option to open in new tab.

<aside class="notice">
The JSON body used to make API request is sent by the end client for availing the respective API service. Hence, some fields may or may not be mandatory.
</aside>
